from time import sleep
from os import system


class Dog():
    def __init__(self, nome, raca, idade=1):
        self.nome = nome
        self.raca = raca
        self.idade = idade
        self.energia = 2

    def latir(self):
        print("Auauauau")

    def andar(self):
        if self.energia >= 1:
            self.energia -= 1
            for x in range(5):
                if x % 2 == 0:
                    print('|\\', end='\r')
                    sleep(1)
                else:
                    print('|', end='\r')
                    sleep(1)
            print()
            return True
        else:
            return False

    def dormir(self):
        self.energia = 2
        for x in range(10):
            if x % 2 == 0:
                print('zZz...', end='\r')
            else:
                print('ZzZ...', end='\r')
            sleep(1)
        print('ZzZ...')
