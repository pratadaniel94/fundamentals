'''
Criar uma classe Conta
Atributos: titular, saldo
Metodos: Sacar, Depositar, Transferir
'''


class Conta:
    def __init__(self, titular, saldo=0):
        self.titular = titular
        self.saldo = saldo

    def sacar(self, valor):
        if self.saldo >= valor:
            self.saldo -= valor
            print('SAQUE titular: {} valor: {}'.format(
                self.titular, self.saldo))
            return True
        else:
            print('saldo insuficiente titular: {}'.format(self.titular))
            return False

    def depositar(self, valor):
        print("DEPOSITO {} titular: {}".format(valor, self.titular))
        self.saldo += valor

    def transferir(self, valor, conta):
        try:
            if not self.sacar(valor):
                raise ValueError('Falha ao realizar transferencia!')
            try:
                conta.depositar(valor)
            except Exception:
                self.depositar(valor)
                raise ValueError('Falha ao realizar transferencia!')
        except ValueError as e:
            print(e)

    def __str__(self):
        return 'Titular: {} Saldo:{}'.format(
            self.titular, self.saldo
        )


class Poupanca(Conta):
    def __init__(self, titular, saldo=0):
        super().__init__(titular, saldo)
        self.juros = 0.005

    def render_juros(self):
        self.saldo += self.saldo * self.juros
