class Pai:
    a = 'classe pai'

class Mae:
    a = 'classe mae'

class Filho(Pai, Mae):
    c = 'classe filho'

classe = Filho()

print(classe.a, classe.c)