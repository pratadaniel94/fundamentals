from pymongo import MongoClient
from pprint import pprint
from faker import Faker
from bson import ObjectId
# pip3 install faker
try:
    con = MongoClient()
    db = con['projeto']
except Exception as e:
    print(e)
    exit()

fake = Faker('pt-BR')

for registro in list(db.usuarios.find()):
    registro['_id'] = str(registro['_id'])
    print(type(registro['_id']))

for x in range(1000):
    usuario = {
        'nome': fake.name(),
        'cpf': fake.cpf(),
        'email': fake.email(),
        'senha': fake.sha256(),
    }
    db.usuarios.insert(usuario)
pprint(list(db.usuarios.find()))
