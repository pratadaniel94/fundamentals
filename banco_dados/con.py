from psycopg2 import connect
from faker import Faker

fake = Faker("pt-BR")

try:
    con = connect(
        'host=127.0.0.1 user=admin2 password=4linux dbname=projeto'
    )
    cur = con.cursor()
    for x in range(100):
        cur.execute("insert into usuarios (nome, email, cpf, senha)\
            values('{}','{}','{}','{}')".format(
            fake.name(), fake.email(), fake.cpf(), fake.sha256()
        ))

except Exception as e:
    if 'con' in globals():
        con.rollback()
    print(e)

finally:
    if 'cur' in globals():
        cur.close()
    if 'con' in globals():
        con.commit()
        con.close()

print('hello world')
