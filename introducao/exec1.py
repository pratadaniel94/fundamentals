#!/usr/bin/python3

ling = input('Qual a melhor linguagem de programação? ')
ling = ling.strip().lower()

try:
    if ling == 'python':
        print('acerto')
    elif ling == 'java' or ling == 'golang':
        print('quase')

    elif ling == 'html' or ling == 'css':
        raise ValueError('Isso não é linguagem de programação!')
except ValueError as e:
    print(e)