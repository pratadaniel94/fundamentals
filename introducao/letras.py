#!/usr/bin/python3
'''criar uma lista de letras de a-z utilizando loop'''
letras = [chr(x).upper() for x in range(97, 123) if chr(x) != 'a']

letras = []
for x in range(97, 123):
    letras.append(chr(x))

print(letras)