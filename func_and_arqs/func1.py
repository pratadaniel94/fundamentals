#!/usr/bin/python3


var = 10

def escopo():
    global var
    var = 5
    print(var)


escopo()
print(var)
del var
# def somar(x,y):
#     return x + y

# a = somar(2,2)
# b = somar(3,3)
# print(somar(a,b))


# def bem_vindo(nome='anonimo'):
#     print('Ola {} seja bem vindo!'.format(nome))


# bem_vindo('daniel')
# bem_vindo('maria')
# bem_vindo('jose')
# bem_vindo())