#!/usr/bin/python3

arquivo= input('Nome do arquivo: ')
arquivo += '.py'
shebang = '#!/usr/bin/python3\n'
try:
    with open(arquivo, 'x') as arq:
        arq.write(shebang)
except FileExistsError:
    with open(arquivo, 'r') as arq:
        if arq.readline() != shebang:
            arq.seek(0)
            conteudo = arq.readlines()
            conteudo.insert(0, shebang)
            with open(arquivo,  'w') as arq:
                arq.writelines(conteudo)