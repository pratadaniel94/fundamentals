#!/usr/bin/python3
'''
ler_arquivo
escrever_arquivo
contar_linhas

'''


def ler_arquivo(arquivo:str)->list:
    '''funcao para ler conteudo de um arquivo
    parm arquivo = nome do arquivo
    '''
    try:
        with open(arquivo, 'r')as arq:
            return arq.readlines()
    except FileNotFoundError:
        return []

def escrever_arquivo(arquivo, conteudo):
    with open(arquivo, 'a')as arq:
        arq.write(conteudo + '\n')


def contar_linhas(arquivo):
    return len(ler_arquivo(arquivo))

   
if __name__  =='__main__':
    print('hello world')
    print('hello world')
    escrever_arquivo(conteudo='teste func', arquivo="teste.txt")


    # conteudo = ler_arquivo('teste.txt')
    # print(conteudo)
    # print(contar_linhas('teste.txt'))

    print(ler_arquivo('outro.txt'))
    print(ler_arquivo.__doc__)